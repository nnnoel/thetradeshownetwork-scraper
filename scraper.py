"""
Base url: https://exhibition-directory.c14.ixsecure.com/ttn/index.php?vShow=&vSort=&BNR=&vInd=&vOrg=&vMo=&vCity=&vCtry=&vRgn=&vExh=&vInd=&vYr=&vState=&vAtt=&vNSF=&vRpP=100&vPos=0

Notes: There are 3996 exhibitions total, so intervals of 100 -- 399 possible page intervals

Columns for spreadsheet:
- Company
- Show
- Date
- Attendees
- Exhbitors

The corresponding <td> classnames are:
- No company name
- r-Name > r-content
- r-Dates > r-content
- r-Att > r-content
- r-Exh > r-content

"""

__author__ = 'noel'

from bs4 import BeautifulSoup
from urllib2 import urlopen
import re
import csv

base_url = 'https://exhibition-directory.c14.ixsecure.com/ttn/index.php?vShow=&vSort=&BNR=&vInd=&vOrg=&vMo=&vCity=&vCtry=&vRgn=&vExh=&vInd=&vYr=&vState=&vAtt=&vNSF=&vRpP=100&vPos='
page_num = 0
fieldnames = ['Show Name', 'Dates', 'Attendees', 'Exhibtors']

namelist = []
datelist = []
attlist = []
exhlist = []
new_list = []

def merge_lists(list1, list2, list3, list4):
	l1 = len(list1)
	counter = 0
	while counter <= l1:
		try:
			new_dict = dict( list1[counter].items() +  list2[counter].items() + list3[counter].items() + list4[counter].items())
			new_list.append(new_dict)
			counter = counter + 1
		except IndexError, e:
			break

def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def setup_bs_and_write():
	openurl = urlopen(base_url + str(page_num)).read()
	soup = BeautifulSoup(openurl, 'html.parser')

	def write_to_csv():
		with open('exhibitions-directory.csv', 'w') as csvfile:
			w = csv.DictWriter(csvfile, fieldnames=fieldnames)
			w.writeheader()

			for data in soup.find_all('td', class_="r-Name"):
				for name in data.find_all('div', class_='r-content'):
					showname = name.find('a').find('u').text
					namelist.append({"Show Name": showname})


			for data in soup.find_all('td', class_="r-Dates"):
				for date in data.find_all('div', class_='r-content'):
					nextdates = date.text
					datelist.append({"Dates": nextdates})


			for data in soup.find_all('td', class_="r-Att"):
				for att in data.find_all('div', class_='r-content'):
					attendee = att.text
					attlist.append({"Attendees": attendee})


			for data in soup.find_all('td', class_="r-Exh"):
				for exh in data.find_all('div', class_='r-content'):
					exhibitor = exh.text
					exhlist.append({"Exhibtors": exhibitor})

			merge_lists(namelist, datelist, attlist, exhlist)

			for _dict in new_list:
				_dict = convert(_dict)
				w.writerow(_dict)

			print 'Processing next page..'

	write_to_csv()

while page_num < 3990:
	setup_bs_and_write()
	page_num = page_num + 100

print 'Done..'

